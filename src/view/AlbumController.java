package view;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;
import javafx.event.ActionEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class AlbumController {
	@FXML Button Login;
	@FXML Button Logout;
	@FXML Button OpenAlbum;
	@FXML Button CloseAlbum;
	@FXML Button Close;
	@FXML Button Slideshow;
	@FXML Button SLClose;
	@FXML Button Next;
	@FXML Button Back;
	@FXML Button Search;
	@FXML Button SearchClose;
	@FXML TextField LoginField;
	@FXML TextField adminField;
	@FXML TextField albumField;
	@FXML TextField captionField;
	@FXML TextField tagSearch;
	@FXML TextArea tagField;
	@FXML ListView<User> userList;
	@FXML ListView<Album> albumList;
	@FXML TilePane Thumbnails;
	@FXML ImageView DisplayArea;
	@FXML ImageView SLMain;
	
	String dirName = "data";
	String extension = ".xml";
	ArrayList<User> users = new ArrayList<User>();
	private ObservableList<User> userObsList;
	private ObservableList<Album> albumObsList;
	private boolean usersListed = false;
	private boolean albumShown = false;
	Photo currentPhoto;
	int SLIndex;
	
	public void start(Stage mainStage) throws IOException {

	}
	
	public void login(ActionEvent e) throws IOException, ClassNotFoundException {
		Stage stage;
		Parent root;
		String username = LoginField.getText();
		File newFolder = new File(dirName);
		if (!newFolder.exists()) {
			newFolder.mkdir();
		}
		
		newFolder = new File("Photos");
		if (!newFolder.exists()) {
			newFolder.mkdir();
		}
		
		if(username.equalsIgnoreCase("Admin")) {
			stage = (Stage)Login.getScene().getWindow();
			root = FXMLLoader.load(getClass().getResource("adminUser.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
		else if(new File(dirName + File.separator + username + extension).isFile()) {
			//dirty workaround
			PrintWriter writer = new PrintWriter("currentUser.txt", "UTF-8");
			writer.print(username);
			writer.close();
					
			stage = (Stage)Login.getScene().getWindow();
			root = FXMLLoader.load(getClass().getResource("regularUser.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
		else {
			showError("User does not exist");
		}
	}
	
	public void logout(ActionEvent e) throws FileNotFoundException, IOException {
		Stage stage;
		Parent root;
		
		stage = (Stage)Logout.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public void listUsers() throws FileNotFoundException, IOException, ClassNotFoundException {
		if (!usersListed) {
			if(new File(dirName + File.separator + "userList" + extension).isFile()) {
				ObjectInputStream ois;
				ois = new ObjectInputStream(new FileInputStream(dirName + File.separator + "userList" + extension));
				users = (ArrayList<User>)ois.readObject();
				userObsList = FXCollections.observableList(users);
				userList.setItems(userObsList);
				ois.close();
			}
		}
		usersListed = true;
	}
	
	public void showList(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		listAlbums(user);
	}
	
	public void listAlbums(User currentUser) throws FileNotFoundException, IOException, ClassNotFoundException {
		ArrayList<Album> albums = currentUser.getAlbumList();
		albumObsList = FXCollections.observableList(albums);
		albumList.setItems(albumObsList);
	}
	
	public void quit(ActionEvent e) throws IOException {
		Platform.exit();
	}
	
	public void saveUserList() throws FileNotFoundException, IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dirName + File.separator + "userList" + extension));
		oos.writeObject(users);
		oos.close();
	}

	public void saveUser(User user) throws FileNotFoundException, IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dirName + File.separator + user.toString() + extension));
		oos.writeObject(user);
		oos.close();
	}
	
	public void addUser(ActionEvent e) throws IOException, ClassNotFoundException {
		if (!usersListed) {
			listUsers();
		}
		
		String username = adminField.getText();
		
		if (username.equals("")) {
			showError("Enter valid username");
			return;
		}
		
		User user = new User(username);
		
		if ((!new File(dirName + File.separator + username + extension).isFile())) {
			//write to file
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dirName + File.separator + username + extension));
			oos.writeObject(user);
			oos.close();
			
			//write to arraylist for display
			users.add(user);
			userObsList = FXCollections.observableList(users);
			userList.setItems(userObsList);
			adminField.setText("");
			saveUserList();
		}
		else {
			showError("User already exists");
		}
	}
	
	public void createAlbum(ActionEvent e) throws FileNotFoundException, IOException, ClassNotFoundException {
		String albumName = albumField.getText();
		User currentUser = retrieveUser();
		
		if (albumName.equals("")) {
			showError("Enter valid album name");
			return;
		}
		
		Album album = new Album(albumName);
		
		if(!(currentUser.duplicateName(albumName))) {
			currentUser.addAlbum(album);
			listAlbums(currentUser);
			saveUser(currentUser);
			albumField.setText("");
		}
		else {
			showError("Album already exists");
		}
	}
	
	public void openAlbum(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		int selectedIndex = albumList.getSelectionModel().getSelectedIndex();
		
		if (selectedIndex >= 0) {
			PrintWriter writer = new PrintWriter("currentAlbum.txt", "UTF-8");
			writer.print(user.getAlbumList().get(selectedIndex).toString());
			writer.close();
			
			Stage stage;
			Parent root;
			stage = (Stage)OpenAlbum.getScene().getWindow();
			root = FXMLLoader.load(getClass().getResource("openAlbum.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

	public void slideshow(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		
		if(album.isEmpty()) {
			showError("Album is empty");
			return;
		}
		
		Stage stage;
		Parent root;
		stage = (Stage)Slideshow.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("slideshow.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public void SLStart(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		
		SLIndex = 0;
		Photo pic = album.getPhoto(SLIndex);
		File photo = new File(pic.getPhotoName());
		Image image = new Image(new FileInputStream(photo), 75, 0, true, true);
		SLMain.setImage(image);
	}
	
	public void SLNext(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		
		if(SLIndex < album.getAlbumSize() - 1) {
			SLIndex++;
			Photo pic = album.getPhoto(SLIndex);
			File photo = new File(pic.getPhotoName());
			Image image = new Image(new FileInputStream(photo), 75, 0, true, true);
			SLMain.setImage(image);
		}
		else {
			showError("End of album");
		}
	}
	
	public void SLBack(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		
		if(SLIndex > 0) {
			SLIndex--;
			Photo pic = album.getPhoto(SLIndex);
			File photo = new File(pic.getPhotoName());
			Image image = new Image(new FileInputStream(photo), 75, 0, true, true);
			SLMain.setImage(image);
		}
		else {
			showError("End of album");
		}
	}
	
	public void slideshowClose(ActionEvent e) throws IOException {
		Stage stage;
		Parent root;
		stage = (Stage)SLClose.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("openAlbum.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public void close(ActionEvent e) throws ClassNotFoundException, IOException {
		Stage stage;
		Parent root;
		stage = (Stage)Close.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("openAlbum.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	//photos must be .jpg
	public void addPhoto(ActionEvent e) throws ClassNotFoundException, IOException {
		currentPhoto = null;
		if(!albumShown) {
			listThumbnails();
		}
		
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		String caption = captionField.getText();
		
		FileChooser fileChooser = new FileChooser();
		configureFileChooser(fileChooser);
		File photoFile = fileChooser.showOpenDialog(Thumbnails.getScene().getWindow());
		
		if (photoFile == null) {
			return;
		}
		
		Photo photo = new Photo(photoFile.getPath(), caption);
		
		if (album.addPhoto(photo)) {
			captionField.setText("");
			String tags = tagField.getText();
			addTags(photo, tags);
			tagField.setText("");
			saveUser(user);
			addToThumbnails(photo);
		}
		else {
			showError("Photo already in album");
		}
	}
	
	public void removePhoto(ActionEvent e) throws ClassNotFoundException, IOException {
		if(currentPhoto == null) {
			return;
		}
		
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		album.deletePhoto(currentPhoto);
		saveUser(user);
		
		Thumbnails.getChildren().clear();
		captionField.setText("");
		tagField.setText("");
		DisplayArea.setImage(null);
		albumShown = false;
		listThumbnails();
		currentPhoto = null;
	}
	
	public void copyPhoto(ActionEvent e) throws ClassNotFoundException, IOException {
		if(currentPhoto == null) {
			showError("Select photo");
			return;
		}
		
		User user = retrieveUser();
		TextInputDialog prompt = new TextInputDialog();
		prompt.setTitle("Copy");
		prompt.setHeaderText("Enter Album");
		prompt.showAndWait();
		String albumDest = prompt.getResult();
		
		if(albumDest.equals("")) {
			showError("Enter valid album name");
		}
		if (copyTo(albumDest, user) == false) {
			showError("Album not found");
		}
		currentPhoto = null;
		saveUser(user);
	}
	
	public void movePhoto(ActionEvent e) throws ClassNotFoundException, IOException {
		if(currentPhoto == null) {
			showError("Select photo");
			return;
		}
		
		User user = retrieveUser();
		Album album = user.getAlbum(retrieveAlbum());
		TextInputDialog prompt = new TextInputDialog();
		prompt.setTitle("Move");
		prompt.setHeaderText("Enter Album");
		prompt.showAndWait();
		String albumDest = prompt.getResult();
		
		if(albumDest.equals("")) {
			showError("Enter valid album name");
		}
		if (copyTo(albumDest, user) == false) {
			showError("Album not found");
		}
		
		album.deletePhoto(currentPhoto);
		currentPhoto = null;
		saveUser(user);
		
		Thumbnails.getChildren().clear();
		captionField.setText("");
		tagField.setText("");
		DisplayArea.setImage(null);
		albumShown = false;
		listThumbnails();
	}
	
	public boolean copyTo(String albumName, User user) throws ClassNotFoundException, IOException {
		Album album = user.getAlbum(retrieveAlbum());
		
		Album albumDest = user.getAlbum(albumName);
		if (albumDest == null)  {
			return false;
		}
		albumDest.addPhoto(currentPhoto);
		return true;
	}
	
	public void configureFileChooser(FileChooser fileChooser) {
		fileChooser.setTitle("Select Photo");
		fileChooser.setInitialDirectory(new File("Photos"));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JPG", "*.jpg"));
	}
	
	public void addToThumbnails(Photo pic) throws FileNotFoundException {
		File photo = new File(pic.getPhotoName());
		Image image = new Image(new FileInputStream(photo), 75, 0, true, true);
		ImageView imageview = new ImageView(image);
		imageview.setOnMouseClicked(e -> {
			try {
				currentPhoto = pic;
				displayData(pic);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		});
		Thumbnails.getChildren().addAll(imageview);
	}
	
	public void addTags(Photo photo, String tagsRaw)  {
		String[] tags = tagsRaw.split(";");
		
		for (int x = 0; x < tags.length; x++) {
			String[] tag = tags[x].split(":");
			if (tag.length == 2) {
				photo.addTag(tag[0], tag[1]);
			}
		}
	}
	
	public void addTag(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		if (currentPhoto == null) {
			return;
		}
		
		String tags = tagField.getText();
		addTags(currentPhoto, tags);
		saveUser(user);
	}
	
	public void updateInfo(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		String caption = captionField.getText();
		String tags = tagField.getText();
		
		currentPhoto.setCaption(caption);
		currentPhoto.clearTags();
		addTags(currentPhoto, tags);
		saveUser(user);
	}
	
	public void displayData(Photo photo) throws FileNotFoundException {
		Image image = new Image(new FileInputStream(new File(photo.getPhotoName())), 100, 0, true, true);
		DisplayArea.setImage(image);
		
		captionField.setText(photo.getCaption());
		tagField.setText(photo.getTags());
	}
	
	public void showAlbum(ActionEvent e) throws ClassNotFoundException, IOException {
		currentPhoto = null;
		listThumbnails();
	}
	
	public void listThumbnails() throws ClassNotFoundException, IOException {
		if (albumShown) {
			return;
		}
		User user = retrieveUser();
		String albumName = retrieveAlbum();
		Album album = user.getAlbum(albumName);
		
		for (int x = 0; x < album.getAlbumSize(); x++) {
			addToThumbnails(album.getPhoto(x));
		}
		albumShown = true;
	}
	
	public void closeAlbumView(ActionEvent e) throws IOException {
		Stage stage;
		Parent root;
		stage = (Stage)CloseAlbum.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("regularUser.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public void search(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		ArrayList<Album> albumlist = user.getAlbumList();
		if (albumlist.size() < 1) {
			showError("No albums");
			return;
		}
		String[] tag = tagSearch.getText().split(":");
		if (tag.length != 2) {
			showError("Invalid tag");
			return;
		}
		
		Album newAlbum = new Album("Result");
		
		for(int x = 0; x < albumlist.size(); x++) {
			Album currentAlbum = albumlist.get(x);
			for (int y = 0; y < currentAlbum.getAlbumSize(); y++) {
				Photo photo = currentAlbum.getPhoto(y);
				if (photo.hasTag(tag[0], tag[1])) {
					newAlbum.addPhoto(photo);
				}
			}
		}
		
		//save results
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("searchResult" + extension));
		oos.writeObject(newAlbum);
		oos.close();
		
		//switch scenes
		Stage stage;
		Parent root;
		
		stage = (Stage)Search.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("tagDateSearch.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public void searchClose(ActionEvent e) throws IOException {
		Stage stage;
		Parent root;
		
		stage = (Stage)SearchClose.getScene().getWindow();
		root = FXMLLoader.load(getClass().getResource("regularUser.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	public User retrieveUser() throws IOException, ClassNotFoundException {
		FileReader reader = new FileReader("currentUser.txt");
		BufferedReader bufferedReader = new BufferedReader(reader);
		String username = bufferedReader.readLine();
		bufferedReader.close();
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dirName + File.separator + username + extension));
		User user = (User)ois.readObject();
		ois.close();
		return user;
	}
	
	public String retrieveAlbum() throws IOException {
		FileReader reader = new FileReader("currentAlbum.txt");
		BufferedReader bReader = new BufferedReader(reader);
		String albumName = bReader.readLine();
		bReader.close();
		reader.close();
		return albumName;
	}
	
	public void showError(String alertText) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
		alert.setHeaderText(alertText);
		alert.showAndWait();
	}
	
	public void deleteUser(ActionEvent e) throws IOException {
		if(users.size() > 0) {
			int selectedIndex = userList.getSelectionModel().getSelectedIndex();
			
			//remove from file
			String username = users.get(selectedIndex).toString();
			File file = new File(dirName + File.separator + username + extension);
			file.delete();
			
			if ((new File(dirName + File.separator + username).isDirectory())) {
				file = new File(dirName + File.separator + username);
				file.delete();
			}
			
			//remove from arraylist
			users.remove(selectedIndex);
			userObsList = FXCollections.observableList(users);
			userList.setItems(userObsList);
			saveUserList();
		}
	}
	
	public void deleteAlbum(ActionEvent e) throws IOException, ClassNotFoundException {
		User user = retrieveUser();
		int selectedIndex = albumList.getSelectionModel().getSelectedIndex();
		if (user.getAlbumList().size() > 0 && selectedIndex >= 0) {
			user.deleteAlbum(selectedIndex);
			saveUser(user);
			listAlbums(user);
		}
	}
	
	public void renameAlbum(ActionEvent e) throws ClassNotFoundException, IOException {
		User user = retrieveUser();
		String name = albumField.getText();
		int selectedIndex = albumList.getSelectionModel().getSelectedIndex();
		if(!(user.duplicateName(name))&&(user.getAlbumList().size() > 0) && !(name.equals("")) && selectedIndex >= 0) {
			user.renameAlbum(selectedIndex, name);
			albumField.setText("");
			saveUser(user);
			listAlbums(user);
		}else {
			showError("Invalid name");
		}
	}
}
