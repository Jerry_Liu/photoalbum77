package model;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable{
	
	private static final long serialVersionUID = 8991927038871505774L;
	
	protected String name;
	protected ArrayList<Album> albumList;

	public User(String name) {
		this.name = name;
		this.albumList = new ArrayList<Album>();
	}
	
	public ArrayList<Album> getAlbumList() {
		return this.albumList;
	}
	
	public Album getAlbum(String name) {
		for (int x = 0; x < albumList.size(); x++) {
			if(albumList.get(x).toString().equals(name)) {
				return albumList.get(x);
			}
		}
		return null;
	}
	
	public void addAlbum(Album album) {
		albumList.add(album);
	}
	
	public void deleteAlbum(int x) {
		albumList.remove(x);
	}
	
	public void renameAlbum(int x, String newName) {
		albumList.get(x).setAlbumName(newName);
	}
	
	public boolean duplicateName(String albumName){

		for (int x = 0; x < this.albumList.size(); x++) {
			if(albumList.get(x).name.equals(albumName)){
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return name;
	}
}
