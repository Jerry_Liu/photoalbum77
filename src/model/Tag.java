package model;

import java.io.Serializable;

public class Tag implements Serializable {

		private static final long serialVersionUID = 8991927038871505774L;
		
		public String type;
		public String value;
		
		public Tag(String tagType, String tagValue){
			this.type = tagType;
			this.value = tagValue;
		}
		
		public boolean isSame(Object t){
			if(t == null || !(t instanceof Tag)){
				return false;
			}
			else{
				Tag tg = ((Tag) t);
				if(this.type.compareTo(tg.type) == 0 && this.value.compareTo(tg.value) == 0){
					return true;
				}else{
					return false;
				}
			}
		}
		
		public String toString(){
			return this.type +":" + value +";";
		}
		
}
