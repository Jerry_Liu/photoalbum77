package model;


import java.util.ArrayList;
import java.util.Calendar;
import java.io.Serializable; 

public class Photo implements Serializable{
	
	private static final long serialVersionUID = 8991927038871505774L;
			
	protected String name;
	protected String caption;
	protected ArrayList<Tag> tags;
	protected Calendar date;
	
	public Photo(String fileName, String caption){
		this.name = fileName;
		this.caption = caption;
		
		this.tags = new ArrayList<Tag>();
		this.date = Calendar.getInstance();
		this.date.set(Calendar.MILLISECOND,0);
	}
	
	public boolean hasTag(String tagType, String tagValue){
		for (int i = 0; i < this.tags.size(); i++) {
			if(tags.get(i).isSame(new Tag (tagType, tagValue))){
				return true;
			}
		}
		return false;
	}
	
	public int tagIndex(String tagType, String tagValue){
		for (int i = 0; i < this.tags.size(); i++) {
			if(tags.get(i).isSame(new Tag (tagType, tagValue))){
				return i;
			}
		}
		return -1;
	}
	
	public void addTag(String tagType, String tagValue){
		if(this.hasTag(tagType, tagValue) == true){
			/*"Tag exists already."*/
		}else{
			this.tags.add(new Tag(tagType, tagValue));
		}
	}
	
	public void deleteTag(String tagType, String tagValue){
		if(this.hasTag(tagType, tagValue) == false){
			this.tags.remove(this.tagIndex(tagType, tagValue));
		}else{
			/*"Tag does not exist."*/
		}
	}
	
	public void clearTags() {
		tags = new ArrayList<Tag>();
	}
	
	public String getPhotoName(){
		return name;
	}
	
	public void setPhotoName(String photoName){
		this.name = photoName;
	}
	
	public String getCaption(){
		return caption;
	}
	
	public void setCaption(String photoCaption){
		this.caption = photoCaption;
	}
	
	public String getTags(){
		String tagList = "";
		for (int i = 0; i < this.tags.size(); i++) {
			tagList += this.tags.get(i).toString() +"\n";
		}
		
		return tagList;
	}

}
