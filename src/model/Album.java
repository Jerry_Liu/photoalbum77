package model;

import java.util.ArrayList;
import java.io.Serializable; 

public class Album implements Serializable{
	
	private static final long serialVersionUID = 8991927038871505774L;
	
	protected String name;
	protected ArrayList<Photo> photos;

	public Album(String albumName){
		this.name = albumName;
		this.photos = new ArrayList<Photo>();
	}
	
	public boolean sameName(String photoName){

		for (int i = 0; i < this.photos.size(); i++) {
			if(photos.get(i).name.equals(photoName)){
				return true;
			}
		}
		return false;
	}
	
	public int photoIndex(Photo photo){
		for (int i = 0; i < this.photos.size(); i++) {
			if(photos.get(i).name.equals(photo.name)){
				return i;
			}
		}
		return -1;
	}
	
	public int getAlbumSize() {
		return photos.size();
	}
	
	public Photo getPhoto(int x) {
		return photos.get(x);
	}
	
	public boolean addPhoto(Photo photo){
		if(sameName(photo.name) == true){
			return false;
		}else{
			this.photos.add(photo);
			return true;
		}
	}
	
	public boolean isEmpty() {
		if (photos.size() < 1) {
			return true;
		}
		return false;
	}
	
	public void deletePhoto(Photo photo){
		if(sameName(photo.name) == true){
			this.photos.remove(this.photoIndex(photo));
		}else{
			//"Photo with that name does not exist."//
		}
	}
	
	public void setAlbumName(String albumName){
		this.name = albumName;
	}
	
	public String toString() {
		return name;
	}
	
}
